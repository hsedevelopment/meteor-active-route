Package.describe({
  name: 'hsed:active-route',
  summary: 'Active route helpers',
  git: 'https://bitbucket.org/hsedevelopment/meteor-active-route',
  version: '2.3.4'
});

Package.onUse(function(api) {
  api.versionsFrom(['2.5']);

  api.use([
    'check',
    'coffeescript',
    'reactive-dict',
    'underscore'
  ]);

  api.use([
    'kadira:flow-router@2.0.0',
    'meteorhacks:flow-router@1.8.0',
    'iron:router@1.0.0',
    'templating'
  ], {weak: true});

  api.export('ActiveRoute');

  api.addFiles('lib/activeroute.coffee');

  api.addFiles('client/helpers.coffee', 'client');
});

Package.onTest(function(api) {
  api.versionsFrom(['2.5']);

  api.use([
    'check',
    'coffeescript',
    'reactive-dict',
    'templating',
    'underscore'
  ]);

  api.use([
    'practicalmeteor:mocha',
    'practicalmeteor:chai',
    'zimme:active-route'
  ]);

  api.addFiles([
    'tests/client/activeroute.coffee',
    'tests/client/helpers.coffee'
  ], 'client');

  api.addFiles('tests/server/activeroute.coffee', 'server');
});
